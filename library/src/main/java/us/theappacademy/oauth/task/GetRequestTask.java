package us.theappacademy.oauth.task;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

import us.theappacademy.oauth.view.ApiDataView;

public class GetRequestTask extends AsyncTask<ApiDataView, Void, String> {
    private ApiDataView apiDataView;

    @Override
    protected String doInBackground(ApiDataView... apiDataViews) {
        String responseString = "";
        apiDataView = apiDataViews[0];
        String apiUrl = apiDataView.getUrlForApiCall();

        try{
            HttpClient httpConnection = new DefaultHttpClient();

            HttpGet getRequest = new HttpGet(apiUrl);
            getRequest.setHeader("Accept", "application/json");

            HttpResponse connectionResponse = httpConnection.execute(getRequest);
            responseString = EntityUtils.toString(connectionResponse.getEntity());
        }
        catch (IOException error) {
            Log.e("Json Object Task", "IOException: " + error);
        }

        return responseString;
    }

    @Override
    protected void onPostExecute(String responseString) {
        apiDataView.onTaskFinished(responseString);
    }

}
