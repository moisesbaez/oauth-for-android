package us.theappacademy.oauth.task;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import us.theappacademy.oauth.OAuthConnection;
import us.theappacademy.oauth.OAuthParameters;
import us.theappacademy.oauth.view.ApiDataView;

public class AccessTokenTask extends AsyncTask<ApiDataView, Void, String> {
    private ApiDataView apiDataView;
    private OAuthConnection oauthConnection;

    @Override
    protected String doInBackground(ApiDataView... apiDataViews) {
        String responseString = "";
        apiDataView = apiDataViews[0];
        oauthConnection = apiDataView.getOAuthConnection();

        OAuthParameters parameters = new OAuthParameters();
        parameters.addParameter("client_id", oauthConnection.getClientID());
        parameters.addParameter("client_secret", oauthConnection.getClientSecret());
        parameters.addParameter("redirect_uri", oauthConnection.getRedirectUrl());
        parameters.addParameter("code", oauthConnection.code);
        parameters.addParameter("grant_type", "authorization_code");

        try {
            HttpClient httpConnection = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(oauthConnection.getAccessTokenUrl());
            postRequest.setHeader("Accept", "application/json");

            String[] parameterNames = parameters.getAllParameterNames();
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            for(String name : parameterNames) {
                pairs.add(new BasicNameValuePair(name, parameters.getValueFromParameter(name)));
            }

            postRequest.setEntity(new UrlEncodedFormEntity(pairs));
            HttpResponse connectionResponse = httpConnection.execute(postRequest);
            responseString = EntityUtils.toString(connectionResponse.getEntity());
        }
        catch(UnsupportedEncodingException error) {
            Log.e("Post Request Task", "UnsupportedEncodingException: " + error);
        }
        catch (IOException error) {
            Log.e("TokenTask", "IOException: " + error);
        }

        return responseString;
    }

    @Override
    protected void onPostExecute(String responseString) {
        apiDataView.onTaskFinished(responseString);
    }
}
