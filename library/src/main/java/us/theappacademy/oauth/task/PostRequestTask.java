package us.theappacademy.oauth.task;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import us.theappacademy.oauth.OAuthParameters;
import us.theappacademy.oauth.view.ApiDataView;

public class PostRequestTask extends AsyncTask<ApiDataView, Void, String> {
    private ApiDataView apiDataView;

    @Override
    protected String doInBackground(ApiDataView... apiDataViews) {
        String responseString = "";
        JSONObject jsonObject = new JSONObject();
        apiDataView = apiDataViews[0];
        String url = apiDataView.getUrlForApiCall();
        OAuthParameters oauthParameters = apiDataView.getOAuthParameters();

        HttpClient httpConnection = new DefaultHttpClient();
        HttpPost postRequest = new HttpPost(url);
        postRequest.setHeader("Accept", "application/json");
        postRequest.setHeader("X-HTTP-Method-Override", apiDataView.getHttpRequestMethod());

        String[] parameterNames = oauthParameters.getAllParameterNames();

        try {
            for(String name : parameterNames) {
                jsonObject.put(name, oauthParameters.getValueFromParameter(name));
            }

            StringEntity stringEntity = new StringEntity(jsonObject.toString());
            postRequest.setEntity(stringEntity);
            HttpResponse connectionResponse = httpConnection.execute(postRequest);

            responseString = EntityUtils.toString(connectionResponse.getEntity());
        }
        catch(UnsupportedEncodingException error) {
            Log.e("Post Request Task", "UnsupportedEncodingException: " + error);
        }
        catch(IOException error) {
            Log.e("Post Request Task", "IOException: " + error);
        }
        catch(JSONException error) {
            Log.e("Post Request Task", "JSONException: " + error);
        }

        return responseString;
    }

    @Override
    protected void onPostExecute(String responseString) {
        apiDataView.onTaskFinished(responseString);
    }


}
