package us.theappacademy.oauth.view;

import us.theappacademy.oauth.OAuthConnection;
import us.theappacademy.oauth.OAuthParameters;

public interface ApiDataView {
    OAuthActivity getParentActivity();

    String getUrlForApiCall();
    void setUrlForApiCall(String urlForApiCall);

    OAuthConnection getOAuthConnection();
    void setOAuthConnection(OAuthConnection oauthConnection);

    OAuthParameters getOAuthParameters();
    void setOAuthParameters(OAuthParameters oauthParameters);

    String getHttpRequestMethod();
    void setHttpRequestMethod(String httpRequestMethod);

    void onTaskFinished(String responseString);
}
