package us.theappacademy.oauth.view;

import android.app.Activity;
import android.app.Fragment;

import org.json.JSONObject;

import us.theappacademy.oauth.OAuthConnection;
import us.theappacademy.oauth.OAuthParameters;

public abstract class OAuthFragment extends Fragment implements ApiDataView {
    private OAuthActivity oauthActivity;
    private OAuthParameters oauthParameters;
    private JSONObject jsonObject;
    private String urlForApiCall;
    private String httpRequestMethod;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        oauthActivity = (OAuthActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        oauthActivity = null;
    }

    @Override
    public OAuthActivity getParentActivity() {
        return oauthActivity;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    @Override
    public String getUrlForApiCall() {
        return urlForApiCall;
    }

    @Override
    public void setUrlForApiCall(String urlForApiCall) {
        this.urlForApiCall = urlForApiCall;
    }

    @Override
    public OAuthConnection getOAuthConnection() {
        return oauthActivity.oauthConnection;
    }

    @Override
    public void setOAuthConnection(OAuthConnection oauthConnection) {
        oauthActivity.oauthConnection = oauthConnection;
    }

    @Override
    public OAuthParameters getOAuthParameters() {
        return oauthParameters;
    }

    @Override
    public void setOAuthParameters(OAuthParameters oauthParameters) {
        this.oauthParameters = oauthParameters;
    }

    public String getHttpRequestMethod() {
        return httpRequestMethod;
    }

    @Override
    public void setHttpRequestMethod(String httpRequestMethod) {
        this.httpRequestMethod = httpRequestMethod;
    }

    @Override
    public abstract void onTaskFinished(String responseString);
}
