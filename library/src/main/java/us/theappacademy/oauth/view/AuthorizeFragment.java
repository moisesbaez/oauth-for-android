package us.theappacademy.oauth.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import org.json.JSONException;
import org.json.JSONObject;

import us.theappacademy.oauth.R;
import us.theappacademy.oauth.util.AuthorizeWebClient;
import us.theappacademy.oauth.util.JsonBuilder;
import us.theappacademy.oauth.util.UrlBuilder;

public class AuthorizeFragment extends OAuthFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getOAuthConnection().state = getOAuthParameters().getValueFromParameter("state");

        String url = UrlBuilder.buildUrlWithParameters(getOAuthConnection().getAuthorizeUrl(), getOAuthParameters());
        setUrlForApiCall(url);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_authorize, container, false);
        WebView authorizeWebView = (WebView)fragmentView.findViewById(R.id.authorizeWebView);
        authorizeWebView.setWebViewClient(new AuthorizeWebClient(this));

        authorizeWebView.loadUrl(getUrlForApiCall());
        return fragmentView;
    }

    @Override
    public void onTaskFinished(String responseString) {
        JSONObject jsonObject = JsonBuilder.jsonObjectFromString(responseString);
        setJsonObject(jsonObject);

        try {
            getOAuthConnection().accessToken = getJsonObject().getString("access_token");
        }
        catch (JSONException error) {
            Log.e("AuthorizeFragment", "JSONException: " + error);
        }

        getParentActivity().setLayoutView();
    }
}
