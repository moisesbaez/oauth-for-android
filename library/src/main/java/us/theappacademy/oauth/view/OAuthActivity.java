package us.theappacademy.oauth.view;

import android.app.Fragment;

import us.theappacademy.oauth.OAuthConnection;

public abstract class OAuthActivity extends FragmentActivity {
    public OAuthConnection oauthConnection;

    public abstract void setLayoutView();
    public abstract void replaceCurrentFragment(Fragment newFragment, boolean addToStack);
}
