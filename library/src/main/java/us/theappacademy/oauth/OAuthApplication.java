package us.theappacademy.oauth;

import java.io.Serializable;

public class OAuthApplication implements Serializable {
    public String clientID;
    public String clientSecret;

    public OAuthApplication(String clientID, String clientSecret) {
        this.clientID = clientID;
        this.clientSecret = clientSecret;
    }
}
