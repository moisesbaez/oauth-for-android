package us.theappacademy.oauth;

import java.io.Serializable;

public abstract class OAuthConnection implements Serializable {
    private OAuthProvider provider;
    private OAuthApplication application;

    public String code;
    public String accessToken;
    public String state;

    protected OAuthConnection() {
        provider = createOAuthProvider();
        application = createOAuthApplication();
    }

    public String getClientID() {
        return application.clientID;
    }

    public String getClientSecret() {
        return application.clientSecret;
    }

    public String getAccessTokenUrl(){
        return provider.accessTokenURL;
    }

    public String getApiUrl() {
        return provider.apiURL;
    }

    public String getAuthorizeUrl() {
        return provider.authURL;
    }

    public String getRedirectUrl() {
        return provider.redirectURL;
    }

    protected abstract OAuthProvider createOAuthProvider();
    protected abstract OAuthApplication createOAuthApplication();
}
