package us.theappacademy.oauth;

import java.io.Serializable;

public class OAuthProvider implements Serializable {
    public String apiURL;
    public String redirectURL;
    public String authURL;
    public String accessTokenURL;

    public OAuthProvider(String apiURL, String authURL, String accessTokenURL, String redirectURL) {
        this.apiURL = apiURL;
        this.authURL = authURL;
        this.accessTokenURL = accessTokenURL;
        this.redirectURL = redirectURL;
    }
}
