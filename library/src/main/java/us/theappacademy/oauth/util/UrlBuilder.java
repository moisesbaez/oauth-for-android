package us.theappacademy.oauth.util;

import java.util.Random;

import us.theappacademy.oauth.OAuthParameters;

public class UrlBuilder {

    public static String buildUrlWithParameters(String url, OAuthParameters oauthParameters) {
        String[] parameterNames = oauthParameters.getAllParameterNames();

        String fullUrl = url + "?";
        for(String name : parameterNames) {
            fullUrl += name + "=" + oauthParameters.getValueFromParameter(name) + "&";
        }

        return fullUrl;
    }

    public static String generateUniqueState(int sizeOfString) {
        String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        String randomString = "";

        Random random = new Random();
        for(int index = 0; index < sizeOfString; index++) {
            randomString += Character.toString(characters.charAt(random.nextInt(characters.length())));
        }

        return randomString;
    }
}
