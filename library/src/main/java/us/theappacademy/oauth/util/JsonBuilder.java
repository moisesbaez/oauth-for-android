package us.theappacademy.oauth.util;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class JsonBuilder {

    public static JSONObject jsonObjectFromString(String jsonString) {
        JSONTokener tokener = new JSONTokener(jsonString);
        JSONObject jsonObject = null;
        try {
             jsonObject = new JSONObject(tokener);
        }
        catch (JSONException error) {
            Log.e("JsonBuilder", "IOException: " + error);
        }
        return jsonObject;
    }
}
