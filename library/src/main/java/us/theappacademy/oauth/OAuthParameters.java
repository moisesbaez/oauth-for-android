package us.theappacademy.oauth;

import java.io.Serializable;
import java.util.HashMap;

public class OAuthParameters implements Serializable {
    private HashMap<String, String> parameters;

    public OAuthParameters() {
        parameters = new HashMap<String, String>();
    }

    public void addParameter(String name, String value) {
        parameters.put(name, value);
    }

    public String getValueFromParameter(String name) {
        return parameters.get(name);
    }

    public String[] getAllParameterNames() {
        String[] parameterNames = new String[parameters.size()];
        parameters.keySet().toArray(parameterNames);
        return parameterNames;
    }
}
