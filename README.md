# OAuth for Android #

Android implementation of the OAuth 2.0 protocol. This is a library that can be integrated into new projects that require authorization to OAuth 2.0 providers. The library is provided under the [Downloads](https://bitbucket.org/pefstem/oauth-for-android/downloads) section.

* * *
## Installation of Library##
Download the latest version of the library from the [Downloads](https://bitbucket.org/pefstem/oauth-for-android/downloads) section and place it within the `app/libs` folder. If the `libs` folder does not exist within the `app` folder, create it.

Open your `build.gradle` file located in the root of your project. Under the `repositories` section located under `allprojects`, insert the following segment of code (do not delete any code, just insert it underneath any code located within `repositories`):
~~~~
flatDir{
	dirs 'libs'
}
~~~~

You now need to edit your `app/build.gradle` file (note that it is a different file from above). Within the `dependencies` section, insert the following code:
~~~~
compile(name: "oauth-x.x.x", ext: "aar")
~~~~
Make sure to replace the x's with the version number of the OAuth library you are using in your project. Resync your project with Gradle and you are now ready to use the library in your project.
* * *
## Usage ##
Visit the [Wiki](https://bitbucket.org/pefstem/oauth-for-android/wiki) for full details on how to use the library to access data from OAuth 2.0 providers.
* * *
## Contributing ##
To contribute to this project perform the following steps:

1. Clone this repository
2. Open the project within Android Studio
3. Create a feature branch in Git/SourceTree
3. Edit the source files located under the `library` folder found within the root of the project
4. Test your source code under `app/src/main/java/us.theappacademy.tester`
5. Commit your feature branch using Git/SourceTree
6. Push your feature branch to this repository
7. Create a pull request to merge your feature into the master branch

**Always commit changes to a feature branch and not the master branch!!!**
* * *
## Contact ##
Moises Baez  
mbaez@pasedfoundation.org