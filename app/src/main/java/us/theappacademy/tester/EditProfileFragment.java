package us.theappacademy.tester;

import us.theappacademy.oauth.OAuthParameters;
import us.theappacademy.oauth.task.PostRequestTask;
import us.theappacademy.oauth.util.UrlBuilder;
import us.theappacademy.oauth.view.OAuthFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class EditProfileFragment extends OAuthFragment {
    private EditText editLocation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OAuthParameters oauthParameters = new OAuthParameters();
        oauthParameters.addParameter("access_token", getOAuthConnection().accessToken);
        String url = UrlBuilder.buildUrlWithParameters(getOAuthConnection().getApiUrl() + "/user", oauthParameters);
        setUrlForApiCall(url);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_profile_edit, container, false);

        editLocation = (EditText)fragmentView.findViewById(R.id.profileEditLocation);
        Button submitEdits = (Button)fragmentView.findViewById(R.id.profileSubmitEdits);

        submitEdits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OAuthParameters oauthParameters = new OAuthParameters();
                oauthParameters.addParameter("location", editLocation.getText().toString());

                setOAuthParameters(oauthParameters);
                setHttpRequestMethod("PATCH");
                new PostRequestTask().execute(EditProfileFragment.this);
            }
        });

        return fragmentView;
    }

    @Override
    public void onTaskFinished(String responseString) {
        getParentActivity().replaceCurrentFragment(new ProfileFragment(), true);
    }
}
