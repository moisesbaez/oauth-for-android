package us.theappacademy.tester;

import us.theappacademy.oauth.OAuthApplication;
import us.theappacademy.oauth.OAuthConnection;
import us.theappacademy.oauth.OAuthProvider;

public class GitHubConnection extends OAuthConnection {
    @Override
    protected OAuthProvider createOAuthProvider() {
        return new OAuthProvider("https://api.github.com",
                "https://github.com/login/oauth/authorize",
                "https://github.com/login/oauth/access_token",
                "https://phunfactory.com");
    }

    @Override
    protected OAuthApplication createOAuthApplication() {
        return new OAuthApplication("c4720a4ed4a2132f2ba7", "7fc74bb08c276d6b14db920faafffac4d2b7e7c2");
    }

}
