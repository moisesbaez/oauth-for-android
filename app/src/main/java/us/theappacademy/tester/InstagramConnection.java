package us.theappacademy.tester;

import us.theappacademy.oauth.OAuthApplication;
import us.theappacademy.oauth.OAuthConnection;
import us.theappacademy.oauth.OAuthProvider;

public class InstagramConnection extends OAuthConnection {
    @Override
    protected OAuthProvider createOAuthProvider() {
        return new OAuthProvider("https://api.instagram.com/",
            "https://api.instagram.com/oauth/authorize",
            "https://api.instagram.com/oauth/access_token",
            "https://localhost");
    }

    @Override
    protected OAuthApplication createOAuthApplication() {
        return new OAuthApplication("ec8c9b83f0434b4e9a98f64fd9a1efde", "13d6449288a44b8b825df72bdb29c56b");
    }
}
