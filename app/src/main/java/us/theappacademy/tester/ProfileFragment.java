package us.theappacademy.tester;

import us.theappacademy.oauth.OAuthParameters;
import us.theappacademy.oauth.task.GetRequestTask;
import us.theappacademy.oauth.util.JsonBuilder;
import us.theappacademy.oauth.util.UrlBuilder;
import us.theappacademy.oauth.view.OAuthFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfileFragment extends OAuthFragment {
    private TextView profileName;
    private TextView profileUsername;
    private TextView profileLocation;
    private Button editProfile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        OAuthParameters oauthParameters = new OAuthParameters();
        oauthParameters.addParameter("access_token", getOAuthConnection().accessToken);

        String url = UrlBuilder.buildUrlWithParameters(getOAuthConnection().getApiUrl() + "/user", oauthParameters);
        setUrlForApiCall(url);
        new GetRequestTask().execute(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_profile, container, false);

        profileName = (TextView)fragmentView.findViewById(R.id.profileName);
        profileUsername = (TextView)fragmentView.findViewById(R.id.profileUsername);
        profileLocation = (TextView)fragmentView.findViewById(R.id.profileLocation);
        editProfile = (Button)fragmentView.findViewById(R.id.profileEdit);

        try {
            if (getJsonObject() != null) {
                profileName.setText(getJsonObject().getString("name"));
                profileUsername.setText(getJsonObject().getString("login"));
                profileLocation.setText(getJsonObject().getString("location"));
            }
        }
        catch(JSONException error) {
            Log.e("Profile Fragment", "JSONException: " + error);
        }

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentActivity().replaceCurrentFragment(new EditProfileFragment(), true);
            }
        });

        return fragmentView;
    }

    @Override
    public void onTaskFinished(String responseString) {
        JSONObject jsonObject = JsonBuilder.jsonObjectFromString(responseString);
        setJsonObject(jsonObject);

        try {
            profileName.setText(getJsonObject().getString("name"));
            profileUsername.setText(getJsonObject().getString("login"));
            profileLocation.setText(getJsonObject().getString("location"));
        }
        catch (JSONException error) {
            Log.e("ProfileFragment", "Profile: " + error);
        }
    }
}
